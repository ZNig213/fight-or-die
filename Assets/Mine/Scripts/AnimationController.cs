﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField]
    float jumpForce;
    Animator anim;
    Rigidbody rb;
    bool grounded, attacking;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {

        anim.SetBool("grounded", grounded);
        anim.SetBool("attacking", attacking);

        if (Input.GetKeyDown(KeyCode.U)&& attacking == false)
        {
            anim.SetTrigger("Kick");
            attacking = true;
        }
        if (Input.GetKeyDown(KeyCode.J) && attacking == false)
        {
            anim.SetTrigger("Punch");
            attacking = true;
        }
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }
        if (grounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetTrigger("Jump");
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }

    }
    private void OnCollisionExit(Collision drt)
    {
        if (drt.gameObject.tag == "Ground")
        {
            grounded = false;
        }
    }
    void ActualJump()
    {
        print("jumper");
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}