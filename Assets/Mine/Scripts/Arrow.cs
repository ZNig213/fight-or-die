﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
[RequireComponent(typeof(Rigidbody))]
public class Arrow : MonoBehaviour {
    [SerializeField] GameObject arrowToPut;
    [SerializeField] float life = 5f, vel;
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = transform.forward * vel;
        life -= Time.deltaTime;
        if (life <= 0)
        {
            Destroy(gameObject);
        }
        
	}
    private void OnCollisionEnter(Collision tarjer)
    {
        Instantiate(arrowToPut, tarjer.contacts[0].point + (arrowToPut.transform.forward * 0.1f), transform.rotation, tarjer.gameObject.transform);
        Destroyable terject = tarjer.gameObject.GetComponent<Destroyable>();
        if (terject != null)
        {
            terject.SartDestroy(1);
        }
        MineLifer lifer = tarjer.gameObject.GetComponent<MineLifer>();
        MineLifer liferp = tarjer.gameObject.GetComponentInParent<MineLifer>();
        if (lifer != null)
        {
            lifer.Damage(1);
        }
        if(liferp != null)
        {
            liferp.Damage(1);
        }
        Destroy(gameObject);


    }
}
